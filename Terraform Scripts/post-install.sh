#!/bin/bash

echo "Creating a tmp file"

wget https://github.com/kgretzky/pwndrop/releases/download/1.0.1/pwndrop-linux-amd64.tar.gz
tar -xvzf pwndrop-linux-amd64.tar.gz
cd pwndrop/

ip=`ifconfig eth0 | grep "inet " | cut -d " " -f 10`
echo '[pwndrop]' > pwndrop_cust.ini
echo 'listen_ip = "'$ip'"' >> pwndrop_cust.ini
echo 'http_port = 80' >> pwndrop_cust.ini
echo 'https_port = 443' >> pwndrop_cust.ini
echo 'data_dir = "./data"' >> pwndrop_cust.ini
echo 'admin_dir = "./admin"' >> pwndrop_cust.ini
echo '' >> pwndrop_cust.ini
echo '[setup]' >> pwndrop_cust.ini
echo 'username = "admin"' >> pwndrop_cust.ini
echo 'password = "supersecretpassword123!"' >> pwndrop_cust.ini  #Change Password
echo 'redirect_url = "http://info.cern.ch"' >> pwndrop_cust.ini
echo 'secret_path = "/pwndrop"' >> pwndrop_cust.ini

session='c2'

tmux new-session -d -s $session

tmux rename-window -t 0 'pwndrop'
tmux send-keys -t 'pwndrop' './pwndrop -no-dns -config ./pwndrop_cust.ini &' C-m C-m

tmux new-window -t $session:1 -n 'netcat'
tmux send-keys -t 'netcat' 'nc -nvlp 3696' C-m C-m

#tmux attach-session -t $session:0