# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "c2_server_linux" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["XXXXXXXXXXXXXXX"]  # Add account ID here
}


# Put your IP here to whitelist it for ssh

variable "access_addr" {
    type    = string
    default = "0.0.0.0/0"

}

resource "aws_security_group" "ssh_group" {
  name        = "ssh_group"
  description = "Allow Port for SSH access"

  # ssh for remote access, might want to lock down to your IP prior to rolling out
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.access_addr]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3696
    to_port     = 3696
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "primary_c2" {
  ami             = data.aws_ami.c2_server_linux.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.ssh_group.name]
  key_name        = "primary-c2-key"

  provisioner "file" {
    source = "~/post_install.sh"
    destination = "/tmp/post_install.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 777 /tmp/post_install.sh",
      "sudo /tmp/post_install.sh",
    ]
  }

  connection {
    type = "ssh"
    user = "ubuntu"
    password = ""
    private_key = file("~/primary-c2-key.pem")
    host = self.public_ip
  }
}

output "IP" {
  value = aws_instance.primary_c2.public_ip
}